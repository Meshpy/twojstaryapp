package com.twojstarypijany.TwojStaryApp.dao;

import com.twojstarypijany.TwojStaryApp.entity.User;

import java.util.List;

public interface UserDao {
    void addUser(User user);
    User getUserByLastName(String inputtedName);
    List<User> getUsers();
    void deleteUser(int userId);
    User getUser(int userId);
}
