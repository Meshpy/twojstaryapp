package com.twojstarypijany.TwojStaryApp.dao;

import com.twojstarypijany.TwojStaryApp.entity.User;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Component
public class UserDaoImpl implements UserDao {

    private EntityManager entityManager;

    @Autowired
    public UserDaoImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    @Override
    public List<User> getUsers() {
        Session session = getSession();
        Query<User> query = session.createQuery("FROM User ORDER BY userId", User.class);
        List<User> users = query.getResultList();

        return users;
    }

    @Override
    public User getUser(int userId) {
        Session session = getSession();
        User user = session.get(User.class, userId);

        return user;
    }

    @Override
    public void deleteUser(int userId) {
        Session session = getSession();
        Query query = session.createQuery("DELETE FROM User WHERE userId = :userId");
        query.setParameter("userId", userId);
        query.executeUpdate();
    }

    @Override
    public void addUser(User user) {
        Session session = getSession();
        session.saveOrUpdate(user);
    }

    @Override
    public User getUserByLastName(String inputtedName) {
        Session session = getSession();
        User user = (User) session.createQuery("FROM User WHERE lastName = :inputtedName")
                .setString("inputtedName", inputtedName)
                .uniqueResult();

        return user;
    }

    private Session getSession() {
        return entityManager.unwrap(Session.class);
    }
}
