package com.twojstarypijany.TwojStaryApp.endpoint;

import com.twojstarypijany.TwojStaryApp.entity.User;
import com.twojstarypijany.TwojStaryApp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/api")
public class UserEndpoint {

    private UserService userService;
    private static Logger logger = Logger.getLogger(UserEndpoint.class.getName());

    @Autowired
    public UserEndpoint(UserService userService) {
        this.userService = userService;
    }


    @RequestMapping(method = RequestMethod.POST, path = "/add")
    public ResponseEntity<User> addUser(@RequestBody User user, UriComponentsBuilder ucb) {
        HttpHeaders headers = new HttpHeaders();
        URI locationUri = ucb.path("/add").build().toUri();
        headers.setLocation(locationUri);

        User userToCheck = userService.getUserByLastName(user.getLastName());
        if (userToCheck != null) {
            logger.info("User already exists in database with last name: " + user.getLastName());
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }

        userService.addUser(user);
        logger.info("Added user with name: " + user.getFirstName() + " " + user.getLastName());
        return new ResponseEntity<>(user, headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/users")
    public ResponseEntity<List<User>> getUsers() {
        List<User> users = userService.getUsers();
        HttpStatus status = HttpStatus.OK;

        if (users == null) {
            logger.info("No users found in database!");
            status = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(users, status);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/users/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable int id) {
        User user = userService.getUser(id);
        HttpStatus status = HttpStatus.NO_CONTENT;

        if (user == null) {
            logger.info("User with " + id + " id not found!");
            return new ResponseEntity<>(status);
        }

        logger.info("Deletes user with " + id + " id.");
        userService.deleteUser(id);
        return new ResponseEntity<>(status);
    }


}