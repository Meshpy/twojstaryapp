package com.twojstarypijany.TwojStaryApp.service;

import com.twojstarypijany.TwojStaryApp.dao.UserDao;
import com.twojstarypijany.TwojStaryApp.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private UserDao userDao;

    @Autowired
    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }


    @Override
    public List<User> getUsers() {
        return userDao.getUsers();
    }


    @Override
    public User getUser(int userId) {
        return userDao.getUser(userId);
    }

    @Override
    public void deleteUser(int userId) {
        userDao.deleteUser(userId);
    }

    @Override
    public void addUser(User user) {
        userDao.addUser(user);
    }

    @Override
    public User getUserByLastName(String inputtedName) {
        return userDao.getUserByLastName(inputtedName);
    }
}
