package com.twojstarypijany.TwojStaryApp.service;

import com.twojstarypijany.TwojStaryApp.entity.User;

import java.util.List;

public interface UserService {
    void addUser(User user);
    User getUserByLastName(String inputtedName);
    List<User> getUsers();
    void deleteUser(int userId);
    User getUser(int userId);
}
